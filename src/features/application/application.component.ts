import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
        <h1>{{title}}</h1>
        <next-to-go></next-to-go>
    `,
    styleUrls: ['application.stylesheet.css']
})
export class ApplicationComponent {
    public title: string = "Next To Go";
}
