import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {ApplicationComponent} from './application.component';
import {NextToGoComponent} from "../next-to-go/next-to-go.component";
import {NextToGoService} from "../next-to-go/next-to-go.service";
import {CapitaliseTransform} from "../utils/capitalise.transform";
import {MomentTransform} from "../utils/moment.transform";

@NgModule({
    declarations: [
        ApplicationComponent,
        NextToGoComponent,
        CapitaliseTransform,
        MomentTransform
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    providers: [NextToGoService],
    bootstrap: [ApplicationComponent]
})
export class ApplicationModule {}
