import {ChangeDetectorRef, Component} from "@angular/core";
import {NextToGoService} from "./next-to-go.service";
import 'rxjs/add/operator/map';
import * as moment from 'moment';
import {Race, Type} from "./race.class";

@Component({
    selector: 'next-to-go',
    templateUrl: 'next-to-go.template.html',
    styleUrls: ['next-to-go.stylesheet.css']
})
export class NextToGoComponent {
    private nextToGoService: NextToGoService;
    private changeDetector: ChangeDetectorRef;
    
    public harnessRaces: Race[] = [];
    public thoroughbredRaces: Race[] = [];
    public greyhoundRaces: Race[] = [];
    public unclassifiedRaces: Race[] = [];
    
    constructor (nextToGoService: NextToGoService, changeDetector: ChangeDetectorRef) {
        this.nextToGoService = nextToGoService;
        this.changeDetector = changeDetector;
        this.updateRaces();
        
        setInterval(() => this.updateRaces(), 60 * 1000);
        setInterval(() => this.detectChanges(), 1000);
    }
    
    public isAboutToBegin = (race: Race) => {
        return race.startTime.diff(moment()) < 2 * 60 * 1000;
    };
    
    public timeTillRace = (race: Race) => {
        return race.startTime.fromNow();
    };
    
    private static oldRaceFilter = (race: Race) => race.startTime > moment();
    
    private detectChanges = () => {
        this.harnessRaces = this.harnessRaces.filter(NextToGoComponent.oldRaceFilter);
        this.thoroughbredRaces = this.thoroughbredRaces.filter(NextToGoComponent.oldRaceFilter);
        this.greyhoundRaces = this.greyhoundRaces.filter(NextToGoComponent.oldRaceFilter);
        this.unclassifiedRaces = this.unclassifiedRaces.filter(NextToGoComponent.oldRaceFilter);
        this.changeDetector.detectChanges();
    };
    
    private updateRaces = () => {
        this.nextToGoService.races.subscribe((races: Race[]) => {
            races = races.filter(NextToGoComponent.oldRaceFilter);
            
            const getRacesForType = (type: Type) => {
                return races.filter((race: Race) => race.type === type);
            };
            
            this.harnessRaces = getRacesForType(Type.HARNESS);
            this.thoroughbredRaces = getRacesForType(Type.THOROUGHBRED);
            this.greyhoundRaces = getRacesForType(Type.GREYHOUND);
            this.unclassifiedRaces = getRacesForType(Type.UNKNOWN);
            this.changeDetector.detectChanges();
        });
    };
}
