import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {Race} from "./race.class";

@Injectable()
export class NextToGoService {
    private static URL = 'https://api.beta.tab.com.au/v1/tab-info-service/racing/next-to-go/races?jurisdiction=NSW';
    private http: Http;
    
    constructor (http: Http) {
        this.http = http;
    }
    
    public get races (): Observable<Race[]> {
        return this.http
            .get(NextToGoService.URL)
            .map(response => {
                return response.json()
                    .races
                    .map(race => Race.fromTabRace(race))
                    .sort((a, b) => a.startTime > b.startTime ? 1 : -1);
            });
    }
}
