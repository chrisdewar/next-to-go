import * as moment from 'moment';

interface Meeting {
    meetingName: string,
    location: string,
    raceType: string
}

export interface TabRace {
    raceStartTime: string,
    raceNumber: number,
    raceDistance: number,
    meeting: Meeting
}

export enum Type {
    GREYHOUND,
    HARNESS,
    THOROUGHBRED,
    UNKNOWN
}

export class Race {
    public readonly startTime: moment.Moment;
    public readonly name: string;
    public readonly number: number;
    public readonly distance: number;
    public readonly type: Type;
    public readonly location: string;
    
    constructor (name: string, startTime: string, number: number, distance: number, type: Type, location: string) {
        name = name[0].toUpperCase() + name.substr(1).toLowerCase();
        this.name = name;
        this.startTime = moment(startTime);
        this.number = number;
        this.distance = distance;
        this.type = type;
        this.location = location;
    }
    
    public static fromTabRace (tabRace: TabRace) {
        let type: Type;
        
        switch (tabRace.meeting.raceType) {
            case 'G': type = Type.GREYHOUND; break;
            case 'H': type = Type.HARNESS; break;
            case 'T': type = Type.THOROUGHBRED; break;
            default: type = Type.UNKNOWN;
        }
        
        return new Race(
            tabRace.meeting.meetingName,
            tabRace.raceStartTime,
            tabRace.raceNumber,
            tabRace.raceDistance,
            type,
            tabRace.meeting.location
        );
    }
}
