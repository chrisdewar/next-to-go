import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'moment'})
export class MomentTransform implements PipeTransform {
    transform (value: moment.Moment): string {
        return value.fromNow();
    }
}
