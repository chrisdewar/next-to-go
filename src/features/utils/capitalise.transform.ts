import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'capitalise'})
export class CapitaliseTransform implements PipeTransform {
    transform(value: string): string {
        return value
            .split(/\s+/)
            .map((value: string) => value[0].toUpperCase() + value.substr(1).toLowerCase())
            .join(' ');
    }
}
