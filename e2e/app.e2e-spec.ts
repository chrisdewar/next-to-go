import { NextToGoPage } from './app.po';

describe('next-to-go App', () => {
  let page: NextToGoPage;

  beforeEach(() => {
    page = new NextToGoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
