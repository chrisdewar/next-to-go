# Next To Go

The application itself is a fairly straight-forward angular 4.0 application written in typescript.

List of customisations to get the desired functionality:

- Creation of the next-to-go feature which includes a component, a service, a template and stylesheet.
- Creation of some utility pipe formatters for use in the template.
- The next-to-go service exposes a getter which returns an observable representative of sorted result of the http API call.
- The next-to-go component consumes the next-to-go service and filters races based on type into some race arrays.
- The next-to-go template consumes the race arrays, and applies some formatting and classes.
- The next-to-go component makes a request to the races service every 60 seconds.
- The next-to-go component updates the UI every second.

Things I would change given the time to spend on it:

- I originally built a moment pipe formatter (it's still in the repo) that was supposed to reformat the time. However I failed to take into account the fact that the pipe formatters won't be run if the data doesn't change. Later I jammed it into a function to get it to do what I wanted, but it's not an elegant solution.
- I would have liked to have spent more time on the moment implementation to make that a bit nicer.
- Strip out Angular CLI dependency - all the tree shaking magic is hidden away and I don't like it.
- Unit Tests
- End to End Tests
- Documentation

**Note: You will have to disable cross-origin restrictions in your browser for this to work with the Tab API.**

Cheers,
Chris

Everything below this point is standard angular documentation.

---

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
